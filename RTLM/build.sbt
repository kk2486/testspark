//import AssemblyKeys._
//
//assemblySettings

name := "RTLM"

version := "0.2"

scalaVersion := "2.11.11"
val sparkVersion = "2.3.0"

lazy val excludeJpountz = ExclusionRule(organization = "net.jpountz.lz4", name = "lz4")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % Provided ,
  "org.apache.spark" %% "spark-core" % sparkVersion % Test classifier "tests",
  "org.apache.spark" %% "spark-sql" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-sql" % sparkVersion % Test classifier "tests",
  "org.apache.spark" %% "spark-catalyst" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-streaming" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion excludeAll(excludeJpountz),
  "org.apache.spark" %% "spark-catalyst" % sparkVersion % Test classifier "tests",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion excludeAll(excludeJpountz),
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc4",
  "org.apache.commons" % "commons-dbcp2" % "2.3.0",
  "joda-time" % "joda-time" % "2.9.7",
  "net.sf.flatpack" % "flatpack" % "3.4.2",
  "jdom" % "jdom" % "1.0",
  "log4j" % "log4j" % "1.2.14",
  "com.typesafe" % "config" % "1.3.1",
  "junit" % "junit" % "4.12" % Test,
  "org.scalatest" % "scalatest_2.11" % "3.0.5" % Test,
  "com.h2database" % "h2" % "1.4.196" % "test"
)

//Excluding due to issues with config loader, also doubles jar size
excludeDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-spray-json",
  "com.typesafe.akka" %% "akka-http-experimental"
)

test in assembly := {}

parallelExecution in Test := false

excludeFilter in unmanagedSources := HiddenFileFilter || "*.conf"

assemblyMergeStrategy in assembly := {
  case PathList("application.conf")             => MergeStrategy.discard
  case PathList("org", "datanucleus", xs @ _*)             => MergeStrategy.discard
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.endsWith("parquet.thrift")       => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case "log4j.properties" => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case "reference.conf"                                    => MergeStrategy.concat
  case PathList("META-INF", _) => MergeStrategy.discard
  case PathList("avro-ipc", _) => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "plugin.xml" => MergeStrategy.concat
  case PathList(ps @ _*) if ps.last endsWith ".xml" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".class" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".dtd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".xsd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".bnd" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".html" => MergeStrategy.first
  case x =>    val old = (assemblyMergeStrategy in assembly).value
    old(x)
}



lazy val sparksharedlibrary = ProjectRef(file("../../testsupportlibspark/SparkSharedLibrary"), "sparksharedlibrary")
lazy val root = Project(id = "RTLM", base = file(".")) dependsOn (sparksharedlibrary)
