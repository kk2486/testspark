package sbtTestBamboo

import org.scalatest._
import org.scalatest.FunSuite

//
class bambooUnitTest_FunSuite extends FunSuite {

  test("is it 27") {
    assert(27 === 27)
  }

  test("is it 28") {
    assert(28 === 28)
  }
  test("is it 29") {
    assert(29 === 29)
  }

  test("is it not 27") {
    assert(37 === 27)
  }

  test("is it not 26") {
    assert(36 === 26)
  }

}
